<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
Route::get('home', 'WelcomeController@index');
Route::get('welcome', 'WelcomeController@index');
Route::get('dashboard', 'WelcomeController@dashboard');

Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::get('auth/login', 'Auth\AuthController@index');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('group', 'ManageController@group');
Route::get('location', 'ManageController@location');
Route::get('add-location', 'ManageController@addLocation');
Route::get('add-group', 'ManageController@addGroup');
Route::get('update-group/{id}', 'ManageController@editGroup');
Route::post('add-group/update', 'ManageController@addGroup');

Route::group(['prefix' => 'log'], function() {

    Route::any('facebook/login', 'SocialController@Facebook');
    Route::any('google/login', 'SocialController@Google');
    Route::any('linkedin/login', 'SocialController@Linkedin');
    Route::any('twitter', 'SocialController@Twitter');


    Route::get('facebook/{mac}/{url}', function($mac, $url) {
        return SocialAuth::authorize('facebook', $mac, $url);
    });


    Route::get('gplus/{mac}/{url}', function($mac, $url) {
        return SocialAuth::authorize('google', $mac, $url);
    });

    Route::get('linkedin/{mac}/{url}', function($mac, $url) {
        return SocialAuth::authorize('linkedin', $mac, $url);
    });



      /*
       * Route::get('twitter', ['as' => 'twitter.login', function(){
        // your SIGN IN WITH TWITTER  button should point to this route
        $sign_in_twitter = true;
        $force_login = false;

        // Make sure we make this request w/o tokens, overwrite the default values in case of login.
        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('twitter.callback'));

        if (isset($token['oauth_token_secret']))
        {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);

            return Redirect::to($url);
        }

        return Redirect::route('twitter.error');
    }]);*/

});

// Generate a login URL
Route::get('/facebook/login', 'SocialController@Facebook');

// Endpoint that is redirected to after an authentication attempt
Route::get('/facebook/callback', 'SocialController@callback');
Route::get('return', 'SocialController@index');


Route::get('twitter/callback', ['as' => 'twitter.callback', function() {
    // You should set this route on your Twitter Application settings as the callback
    // https://apps.twitter.com/app/YOUR-APP-ID/settings
    if (Session::has('oauth_request_token'))
    {
        $request_token = [
            'token'  => Session::get('oauth_request_token'),
            'secret' => Session::get('oauth_request_token_secret'),
        ];

        Twitter::reconfig($request_token);

        $oauth_verifier = false;

        if (Input::has('oauth_verifier'))
        {
            $oauth_verifier = Input::get('oauth_verifier');
        }

        // getAccessToken() will reset the token for you
        $token = Twitter::getAccessToken($oauth_verifier);

        if (!isset($token['oauth_token_secret']))
        {
            return Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
        }

        $credentials = Twitter::getCredentials();

        if (is_object($credentials) && !isset($credentials->error))
        {
            // $credentials contains the Twitter user object with all the info about the user.
            // Add here your own user logic, store profiles, create new users on your tables...you name it!
            // Typically you'll want to store at least, user id, name and access tokens
            // if you want to be able to call the API on behalf of your users.

            // This is also the moment to log in your users if you're using Laravel's Auth class
            // Auth::login($user) should do the trick.

            Session::put('access_token', $token);

            return Redirect::to('/')->with('flash_notice', 'Congrats! You\'ve successfully signed in!');
        }

        return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
    }
}]);