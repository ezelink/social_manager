<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ManageService;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ManageController extends Controller
{
    protected $manageservice;


    /***
     * @param ManageService $manageService
     */
    public function __construct(ManageService $manageservice)
    {
        $this->manageservice = $manageservice;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function group()
    {
        $groups =  $this->manageservice->getGroups();

        if($groups !== false){
            $data = [
                'groups' => $groups,
            ];
        }else{
            $data = [
                'groups' => array()
            ];
        }

        return view('manage.group', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editGroup($id)
    {
        $group_data =  $this->manageservice->getGroupData($id);

        $data = [
            'group_data' => $group_data
        ];
        return view('manage.group_add', $data);
    }


    /***
     * @return \Illuminate\View\View
     */
    public function location()
    {
        $groups =  $this->manageservice->getGroups();
        if($groups !== false){
            $data = [
                'groups' => $groups,
            ];
        }else{
            $data = [
                'groups' => array()
            ];
        }

        return view('manage.location', $data);
    }


    /***
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function addGroup(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make(
                [
                    'name'=>Input::get('name'),
                    'ssid'=>Input::get('ssid')
                ],
                [
                    'name' => 'required',
                    'ssid' => 'required',
                ]
            );


            if ($validator->fails()) {
                return Redirect::to('group')->withInput()->withErrors($validator);

            }else{
                $name   = Input::get('name');
                $ssid   = Input::get('ssid');

                try {
                    $add_status = $this->manageservice->AddGroup($name, $ssid);
                    if ($add_status == true) {

                        Session::flash('flash_message', '<strong>New Group Added Successfully.</strong>');
                        Session::flash('flash_type', 'success');
                    }else{
                        Session::flash('flash_message', '<strong>Your Request Not Successfull.</strong>');
                        Session::flash('flash_type', 'error');
                    }

                }
                catch (\Exception $ex) {
                    Session::flash('flash_message', $ex->getMessage());
                    Session::flash('flash_type', 'error');
                }
                return redirect('/group');

            }

        }else{
            return view('manage.group_add');
        }
    }


    /***
     * @return \Illuminate\View\View
     */
    public function addLocation()
    {


        return view('manage.location_add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
