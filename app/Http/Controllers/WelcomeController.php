<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Redirect;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->middleware('auth');
	}


	/***
	 * @return \Illuminate\View\View
	 */
	public function dashboard()
	{
		return view('welcome');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $user_mail = Auth::user()->email;
        if($user_mail && $user_mail == 'asfaransl@gmail.com'){

            return view('welcome');

        }else{
            return Redirect::to('/return');
        }


	}

}
