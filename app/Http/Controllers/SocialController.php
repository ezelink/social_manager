<?php

namespace App\Http\Controllers;
/*require 'Facebook\autoload.php';*/
use Auth;
use App\User;
use Redirect;
use SocialAuth;
use Twitter;
use AdamWathan\EloquentOAuth\Facades\OAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Facebook\Facebook;
use Facebook\FacebookSession;
use BaseFacebook;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Support\Facades\Session;
use AdamWathan\EloquentOAuthL5\EloquentOAuthServiceProvider;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;
use Illuminate\Contracts\Auth\Guard;
use Symfony\Component\Process\Process;


class SocialController extends Controller
{

    /***
     * @return mixed
     */
    public function Facebook()
    {
        OAuth::login('facebook', function ($user, $userDetails) {


            $user_specific = User::where('email',$userDetails->email)->first();
            if(!$user_specific){

                $user->email = $userDetails->email;
                $user->social_user_id = $userDetails->id;
                $user->name = $userDetails->full_name;
                $user->first_name = $userDetails->raw['first_name'];
                $user->last_name = $userDetails->raw['last_name'];
                $user->save();
            }

        });
        return Redirect::to('/return');
    }

    /***
     * @return mixed
     */
    public function Google()
    {
        OAuth::login('google', function ($user, $userDetails) {

            $user_specific = User::where('email', $userDetails->email)->first();

            if(!$user_specific){

                $user->email = $userDetails->email;
                $user->social_user_id = $userDetails->id;
                $user->name = $userDetails->full_name;
                $user->first_name = $userDetails->raw['given_name'];
                $user->last_name = $userDetails->raw['family_name'];
                $user->save();
            }

            //dd($userDetails->email);
        });

        return Redirect::to('/return');
    }

    /***
     * @return mixed
     */
    public function Linkedin()
    {
        OAuth::login('linkedin', function ($user, $userDetails) {

            $user_specific = User::where('email',$userDetails->email)->first();

            if(!$user_specific){

                $user->email = $userDetails->email;
                $user->social_user_id = $userDetails->id;
                $user->name = $userDetails->full_name;
                $user->first_name = $userDetails->raw['firstName'];
                $user->last_name = $userDetails->raw['lastName'];
                $user->save();
            }

            //dd($userDetails->email);
        });

        return Redirect::to('/return');
    }


    /***
     * @return mixed
     */
    public function Twitter()
    {
        // your SIGN IN WITH TWITTER  button should point to this route
        $sign_in_twitter = true;
        $force_login = false;

        // Make sure we make this request w/o tokens, overwrite the default values in case of login.
        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('twitter.callback'));

        if (isset($token['oauth_token_secret']))
        {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return Redirect::to($url);
        }

        return Redirect::route('twitter.error');
    }


    /***
     * @param LaravelFacebookSdk $fb
     * @return mixed
     */
    public function callback(LaravelFacebookSdk $fb)
    {
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Access token will be null if the user denied the request
        // or if someone just hit this URL outside of the OAuth flow.
        if (! $token) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if (! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

        // Save for later
        Session::put('fb_user_access_token', (string) $token);

        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,email');
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();

        // Create the user if it does not exist or update the existing entry.
        // This will only work if you've added the SyncableGraphNodeTrait to your User model.
        if($facebook_user){

            if($facebook_user['email']){

                $user_data = User::where('email', '=', $facebook_user['email'])->first();

                if ($user_data === null) {
                    $user = User::createOrUpdateGraphNode($facebook_user);
                    Auth::login($user);
                }else{
                    Session::flash('flash_message', 'Successfully logged in with Facebook.');
                    Session::flash('flash_type', 'success');
                    return redirect('return');
                }
            }

        }
        // Log the user into Laravel
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LaravelFacebookSdk $fb)
    {
        $device_mac = Session::get('device_mac');
        $login_return_url = Session::get('login_return_url');

        return Redirect::to("http://" . $login_return_url. '/login/success.php?mac='. $device_mac);

        /*$process = new Process('sudo /sbin/ezespot_query authorize mac '.$device_mac.'  sessiontimeout 1800 ');
        $process->mustRun();
        //$exec = exec( "ezespot_query authorize mac ".$mac_address."  sessiontimeout 240  maxoctets 1000000 ", $output, $return );


        $data = [
            'device_mac' => $device_mac,
        ];
        return view('social.facebook', $data);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}