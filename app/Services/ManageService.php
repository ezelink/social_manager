<?php
/**
 * Created by PhpStorm.
 * User: asfaran
 * Date: 5/25/15
 * Time: 11:33 AM
 */

namespace App\Services;

use DB;
use Auth;
class ManageService
{

    /***
     * @return array|bool
     */
    public function getGroups()
    {
        $return_array = array();
        $groups = DB::select("SELECT groups.id ,
                                    groups.name as group_name
                                    FROM    groups ");
        foreach($groups as $key => $value){
            $return_array[$value->id]['name'] = $value->group_name;
        }
        $found = DB::select("SELECT groups.id as group_id,
                                    groups.name as group_name,
                                    locations.id as loc_id,
                                    locations.name as loc_name
                                    FROM    locations
                                    LEFT JOIN groups
                                      ON locations.group_id = groups.id ");
        foreach($found as $key => $values){
            $return_array[$values->group_id]['array'][$values->loc_id] = $values;
        }
        if (count($return_array) > 0) {
            return $return_array;
        }else{
            return false;
        }

    }


    /**
     * @param $id
     * @return mixed
     */
    public function getGroupData($id)
    {

        $group = DB::select("SELECT * FROM groups WHERE id = ".$id);
        return $group[0];
    }


    /***
     * @param $name
     * @param $ssid
     * @return mixed|string
     */
    public function AddGroup($name, $ssid)
    {
        DB::table('groups')->insert(['name' => $name, 'ssid' => $ssid]);

    }




}




