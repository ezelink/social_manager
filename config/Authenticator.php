<?php namespace AdamWathan\EloquentOAuth;

use App\User;
use Illuminate\Support\Facades\Session;

class Authenticator
{
    protected $auth;
    protected $users;
    protected $identities;

    public function __construct($auth, $users, $identities)
    {
        $this->auth = $auth;
        $this->users = $users;
        $this->identities = $identities;
    }

    public function login($providerAlias, $userDetails, $callback = null)
    {
        $user = $this->getUser($providerAlias, $userDetails);
        if ($callback) {
            $callback($user, $userDetails);
        }
        $this->updateUser($user, $providerAlias, $userDetails);
        $this->auth->login($user);
    }

    protected function getUser($providerAlias, $details)
    {

        if ($this->identities->userExists($providerAlias, $details)) {
            return $this->getExistingUser($providerAlias, $details);
        }else{
            $user_specific = User::where('email', $details->email)->first();
            if($user_specific == null){
                return $this->users->create();
            }else{
                return $user_specific;
            }
        }

    }

    protected function updateUser($user, $providerAlias, $details)
    {
        $this->users->store($user);
        $this->storeProviderIdentity($user, $providerAlias, $details);
    }

    protected function getExistingUser($providerAlias, $details)
    {
        $identity = $this->identities->getByProvider($providerAlias, $details);
        //dd($identity);
        return $this->users->findByIdentity($identity);
    }

    protected function storeProviderIdentity($user, $providerAlias, $details)
    {
        if ($this->identities->userExists($providerAlias, $details)) {
            $this->updateProviderIdentity($providerAlias, $details);
        } else {
            $this->addProviderIdentity($user, $providerAlias, $details);
        }
    }

    protected function updateProviderIdentity($providerAlias, $details)
    {
        $identity = $this->identities->getByProvider($providerAlias, $details);
        $identity->access_token = $details->access_token;
        $this->identities->store($identity);
    }

    protected function addProviderIdentity($user, $providerAlias, $details)
    {
        $identity = new OAuthIdentity;
        $identity->user_id = $user->getKey();

        if($providerAlias == 'facebook'){
            $identity->firstName = $details->raw['first_name'];
            $identity->lastName  = $details->raw['last_name'];

        }elseif($providerAlias == 'google'){
            $identity->firstName = $details->raw['given_name'];
            $identity->lastName  = $details->raw['family_name'];

        }elseif($providerAlias == 'linkedin'){
            $identity->firstName = $details->raw['firstName'];
            $identity->lastName  = $details->raw['lastName'];

        }



        $identity->mac = Session::get('device_mac');
        $identity->pictureUrl = $details->avatar;
        $identity->email = $providerAlias;
        $identity->provider = $providerAlias;
        $identity->provider_user_id = $details->id;
        $identity->access_token = $details->access_token;


        $this->identities->store($identity);
    }
}
