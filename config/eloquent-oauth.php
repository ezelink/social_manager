<?php
	$currentPath = $_SERVER['PHP_SELF'];
	// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
	$pathInfo = pathinfo($currentPath);
	// output: localhost
	$hostName = "";
	if (isset($_SERVER['HTTP_HOST'])) {
		$hostName = $_SERVER['HTTP_HOST'];
	}
	// output: http://

	if (isset($_SERVER['HTTP_HOST'])) {
		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
	}else{
		$protocol = 'http://';
	}

	// return: http://localhost/myproject/
	$path =  $protocol.$hostName;

	return [
		'table' => 'oauth_identities',
		'providers' => [
			'facebook' => [
				'client_id' => '475159279314149',
				'client_secret' => 'f1ea8173d8d3f7e39f65fafcd4a0a50a',
				'redirect_uri' =>  $path.'/soclogin/log/facebook/login',
				'scope' => [],
			],
			'google' => [
				'client_id' => '79092786320-0voo4hvitjlln8cpkhknl3khfng1sks8.apps.googleusercontent.com',
				'client_secret' => 'ZOIDnooy-claCe2b_SFNHkqo',
				'redirect_uri' => $path.'/soclogin/log/google/login',
				'scope' => [],
			],
			'github' => [
				'client_id' => '12345678',
				'client_secret' => 'y0ur53cr374ppk3y',
				'redirect_uri' => 'https://example.com/your/github/redirect',
				'scope' => [],
			],
			'linkedin' => [
				'client_id' => '77l15zqgn6w7uo',
				'client_secret' => 'irDJviC7GESxhAZ9',
				'redirect_uri' => $path .'/soclogin/log/linkedin/login',
				'scope' => [],
			],
			'instagram' => [
				'client_id' => '12345678',
				'client_secret' => 'y0ur53cr374ppk3y',
				'redirect_uri' =>  $path.'/soclogin/instagram/redirect',
				'scope' => [],
			],
			'soundcloud' => [
				'client_id' => '12345678',
				'client_secret' => 'y0ur53cr374ppk3y',
				'redirect_uri' => 'https://example.com/your/soundcloud/redirect',
				'scope' => [],
			],
		],
	];