<?php namespace AdamWathan\EloquentOAuth;
use Illuminate\Support\Facades\Session;

class OAuthManager
{
    protected $redirect;
    protected $authenticator;
    protected $socialnorm;

    public function __construct($redirect, $authenticator, $socialnorm)
    {
        $this->redirect = $redirect;
        $this->authenticator = $authenticator;
        $this->socialnorm = $socialnorm;
    }

    public function authorize($providerAlias, $mac, $url)
    {
        Session::put('device_mac', $mac);
        Session::put('login_return_url', $url);
        return $this->redirect->to($this->socialnorm->authorize($providerAlias));
    }

    public function login($providerAlias, $callback = null)
    {
        $details = $this->socialnorm->getUser($providerAlias);
        return $this->authenticator->login($providerAlias, $details, $callback);
    }

    public function registerProvider($alias, $provider)
    {
        $this->socialnorm->registerProvider($alias, $provider);
    }
}
