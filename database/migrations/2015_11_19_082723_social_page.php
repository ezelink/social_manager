<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_page', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('location_id');
            $table->string('social');
            $table->string('page_id');
            $table->string('page_url');
            $table->string('scope');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_page');
    }
}
