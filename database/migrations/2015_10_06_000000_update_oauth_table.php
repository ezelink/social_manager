<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOauthTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = Config::get('eloquent-oauth.table');

        Schema::table($tableName,function(Blueprint $table){
            $table->text('email')->after('user_id')->nullable();
            $table->text('firstName')->after('provider')->nullable();
            $table->text('lastName')->after('firstName')->nullable();
            $table->text('pictureUrl')->after('lastName')->nullable();
            $table->text('mac')->after('pictureUrl')->nullable();
            $table->text('device')->after('mac')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
