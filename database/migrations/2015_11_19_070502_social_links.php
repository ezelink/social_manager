<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('location_id');
            $table->string('social');
            $table->string('client_id');
            $table->string('client_secret');
            $table->string('redirect_url');
            $table->string('ip');
            $table->string('redirect_folder');
            $table->string('scope');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_links');
    }
}
