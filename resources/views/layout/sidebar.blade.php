<!-- BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse" style="position:fixed">

    <ul class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li  {{ Request::is('dashboard') ? ' class=start active' : ' class=start'}}>
            <a href="{{URL::to('/dashboard')}}">
                <i class="fa fa-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li></li>
    </ul>
</div>
<!-- END SIDEBAR -->