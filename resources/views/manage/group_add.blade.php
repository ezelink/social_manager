<?php
define('PAGE_PARENT', 'management', true);
define('PAGE_CURRENT', 'group', true);
?>
@extends('app')

@section('title', 'Add Groups')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title" xmlns="http://www.w3.org/1999/html">
    Management
    <small>Manage Group</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#">Management</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <i class="fa fa-home"></i>
            <a href="group/">Manage Group</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="">Add Group</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class=""></i>Add Group
                </div>
            </div>
            <div class="portlet-body form">
                {{--@include('partials.messages')--}}
                <form action="{{URL::to('add-group/update') }}" method="post" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ (isset($group_data->id))? $group_data->id  :"" }}">
                    <input type="hidden" name="id" value="">
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                                <div class="col-md-6" >
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label>
                                        <div class="col-md-4">
                                            <input class="form-control" value="{{ (isset($group_data->name))? $group_data->name :"" }}" placeholder="Group Name" type="text" name="name" id="name" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">SSID</label>
                                        <div class="col-md-4">
                                            <input class="form-control" value="{{ (isset($group_data->ssid))? $group_data->ssid :"" }}" placeholder="Group SSID" type="text" name="ssid" id="ssid" />
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="col-md-6" >
                                    <div id="map_canvas" class="map rounded"   style="width:500px; height:350px"></div>
                                </div>--}}
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                @if(isset($group_data))
                                    <button type="submit" class="btn blue" name="btn_add">Update</button>
                                @else
                                    <button type="submit" class="btn blue" name="btn_add">Add Group</button>
                                @endif
                                <button type="reset" onclick="window.location ='{{URL::to('/group') }}'; return false;" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
@endsection