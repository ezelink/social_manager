<?php
define('PAGE_PARENT', 'management', true);
define('PAGE_CURRENT', 'group', true);
?>
@extends('app')

@section('title', 'Manage Groups')

@section('content')
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title" xmlns="http://www.w3.org/1999/html">
    Management
    <small>Manage Group</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <i class="fa fa-home"></i>
            <a href="#">Management</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="group/">Manage Group</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class=""></i>Manage Group
                </div>
                <div class="actions">
                    <a href="{{ url('/add-group') }}" class="btn btn-default btn-sm">
                        <i class="fa fa-plus"></i> Add New Group</a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_1" class="tree-demo">
                    @if(count($groups) > 0)
                        <ul>
                            @foreach($groups as $key => $value)
                                <li data-jstree='{ "type" : "file" }'>
                                    <a href="/update-group/{{  $key  }}" > {!!  $value['name'] !!}</a>
                                    @if(isset($value['array']))
                                        <ul>
                                            @foreach($value['array'] as $key => $value)
                                                <li data-jstree='{ "icon" : "fa  fa-map-marker icon-state-success " }'>
                                                    {!!  $value->loc_name !!}
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
@endsection