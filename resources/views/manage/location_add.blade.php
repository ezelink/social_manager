<?php
define('PAGE_PARENT', 'management', true);
define('PAGE_CURRENT', 'location', true);
?>
@extends('app')

@section('title', 'Add Location')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title" xmlns="http://www.w3.org/1999/html">
    Management
    <small>Manage Location</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#">Management</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <i class="fa fa-home"></i>
            <a href="location/">Location</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="add-location/">add Location</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->


<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet blue-hoki box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Add a  Location
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_1" class="tree-demo">
                    @if(count($groups) > 0)
                        <ul>
                            @foreach($groups as $key => $value)
                                <li data-jstree='{ "type" : "file" }'>
                                    {!!  $value['name'] !!}
                                    @if(count($value['array']) > 0)
                                        <ul>
                                            @foreach($value['array'] as $key => $value)
                                                <li data-jstree='{ "icon" : "fa  fa-map-marker icon-state-success " }'>
                                                    {!!  $value->loc_name !!}
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection