<!-- BEGIN REGISTRATION FORM -->
<form class="register-form" role="form"  action="{{ url('/auth/login') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-title">
        <span class="form-title">Sign Up</span>
    </div>
    <p class="hint">
        Enter your personal details below:
    </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="name"/>
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Address</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="address"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">City/Town</label>
        <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="city"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Country</label>
        <select name="country" class="form-control">
            <option value="">Country</option>
            <option value="AU">Australia</option>
            <option value="AT">Austria</option>
            <option value="BN">Brunei Darussalam</option>
            <option value="BG">Bulgaria</option>
            <option value="BF">Burkina Faso</option>
            <option value="BI">Burundi</option>
            <option value="KH">Cambodia</option>
            <option value="CM">Cameroon</option>
            <option value="CA">Canada</option>
            <option value="EG">Egypt</option>
            <option value="FJ">Fiji</option>
            <option value="FI">Finland</option>
            <option value="FR">France</option>
            <option value="HK">Hong Kong</option>
            <option value="JM">Jamaica</option>
            <option value="JP">Japan</option>
            <option value="KW">Kuwait</option>
            <option value="MW">Malawi</option>
            <option value="MY">Malaysia</option>
            <option value="MV">Maldives</option>
            <option value="ML">Mali</option>
            <option value="MA">Morocco</option>
            <option value="OM">Oman</option>
            <option value="PK">Pakistan</option>
            <option value="PH">Philippines</option>
            <option value="QA">Qatar</option>
            <option value="RU">Russian Federation</option>
            <option value="ES">Spain</option>
            <option value="LK">Sri Lanka</option>
            <option value="SZ">Swaziland</option>
            <option value="SE">Sweden</option>
            <option value="CH">Switzerland</option>
            <option value="SY">Syrian Arab Republic</option>
            <option value="TH">Thailand</option>
            <option value="TR">Turkey</option>
            <option value="UA">Ukraine</option>
            <option value="AE">United Arab Emirates</option>
            <option value="GB">United Kingdom</option>
            <option value="US">United States</option>
            <option value="YE">Yemen</option>
        </select>
    </div>
    <p class="hint">
        Enter your account details below:
    </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password"/>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword"/>
    </div>
    <div class="form-group margin-top-20 margin-bottom-20">
        <label class="check">
            <input type="checkbox" name="tnc"/>
            <span class="loginblue-font">I agree to the </span>
            <a href="javascript:;" class="loginblue-link">Terms of Service</a>
            <span class="loginblue-font">and</span>
            <a href="javascript:;" class="loginblue-link">Privacy Policy </a>
        </label>
        <div id="register_tnc_error">
        </div>
    </div>
    <div class="form-actions">
        <button type="button" id="register-back-btn" class="btn btn-default">Back</button>
        <button type="submit" id="register-submit-btn" class="btn btn-primary uppercase pull-right">Submit</button>
    </div>
</form>
<!-- END REGISTRATION FORM -->